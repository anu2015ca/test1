package bitbucket2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLConnectTest {


	public static void  main(String[] args) throws  ClassNotFoundException, SQLException {													
		//Connection URL Syntax: "jdbc:mysql://ipaddress:portnumber/db_name"		
		String dbUrl = "jdbc:mysql://107.170.213.234:3306/itech";		//SSH HostName= 107.170.213.234			

		//Database Username		
		String username = "qatester";	

		//Database Password		
		String password = "qatester";				

		//Query to Execute	
		String query = "select * from customers";
		//String query = "select * from customers where customers_email_address = \"ecalix@gmail.com\";";

		//Load mysql jdbc driver		
		Class.forName("com.mysql.jdbc.Driver");			

		//Create Connection to DB		
		Connection con = DriverManager.getConnection(dbUrl,username,password);

		//Create Statement Object		
		java.sql.Statement stmt = con.createStatement();		


		// Execute the SQL Query. Store results in ResultSet		
		ResultSet rs= stmt.executeQuery(query);							

		/* While Loop to iterate through all data and print results		
		while (rs.next()){
			String Name = rs.getString(1);								        
			String password = rs.getString(2);					                               
			System. out.println(Name+"  "+ password);		
		}*/		
		// closing DB Connection		
		con.close();			
	}
}

